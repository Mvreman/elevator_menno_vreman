<img src="./src/assets/img/elevator.png" alt="">

# Elevator project Menno Vreman

A basic elevator can have 1 or 2 buttons on the outside to be called from since some elevator types can be more effective if they know whether you are going up or down. Inside the cabin there should be numbered buttons for all floors.

### Installation

```
npm install
```

### Getting started

```
npm start
```
