import React from 'react';
import { connect } from 'react-redux';
import { lift } from '../../redux/actions/';
import './button.css';

const Button = ({value, lift, name}) => {

    return (
        <div className="button-panel">
            <button className="metal linear"
                //calls function life with value e in redux/actions
                    onClick={(e) => lift(e)}
                    name={name}>
                {value}
            </button>
        </div>
    )
};

export default connect(null, { lift })(Button);
