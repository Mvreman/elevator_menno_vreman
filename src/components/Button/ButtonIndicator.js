import React from 'react';
import { connect } from 'react-redux';
import { lift } from '../../redux/actions/';
import './button.css';

const Button = ({value, lift, name}) => {

    return (
            <button className="indicator"
                //calls function life with value e in redux/actions
                    onClick={(e) => lift(e)}
                    name={name}>
                    Call elevator on floor {value}
            </button>
    )
};

export default connect(null, { lift })(Button);
