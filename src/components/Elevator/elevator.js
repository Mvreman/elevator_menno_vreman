import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import './elevator.css';


const Elevator = ({ floor }) => {

  useEffect(() => {
    // makes const person invocable as className
    const person = document.querySelector('.person');
    // look witch state the elevator has and displays the case
    switch (floor) {
      case '0':
        person.style.bottom = 60 + 'px';
        break;
      case '1':
        person.style.bottom = 110 + 'px';
        break;
      case '2':
        person.style.bottom = 160 + 'px';
        break;
      case '3':
        person.style.bottom = 210 + 'px';
        break;
      case '4':
        person.style.bottom = 260 + 'px';
        break;
      case '5':
        person.style.bottom = 310 + 'px';
        break;
      case '6':
        person.style.bottom = 360 + 'px';
        break;
      case '7':
        person.style.bottom = 410 + 'px';
        break;
      case '8':
        person.style.bottom = 460 + 'px';
        break;
      case '9':
        person.style.bottom = 510 + 'px';
        break;
      default:
        person.style.bottom = 60 + 'px';

    }
  }, [floor]);
  //returns this ellement to the App.js
  return (
      <div className="person">{floor}</div>
  )
};


const mapStateToProps = (state) => ({
  floor: state.btnInputReducer.floor,
});

export default connect(mapStateToProps)(Elevator);
