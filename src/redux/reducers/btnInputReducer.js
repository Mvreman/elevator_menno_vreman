// sets default value to 0
const initialState = {
  floor: '0'
};

// calls state of the buttons that's pressed and gives the correct data back
const btninputReducer = (state = initialState, action) => {
  if (action.type === 'RAISE') {
    return {
      ...state,
      floor: action.name,
    };
  } else {
    return state;
  }
};

export default btninputReducer;
