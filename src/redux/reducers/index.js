import { combineReducers } from 'redux';
import btnInputReducer from './btnInputReducer';

const reducers = combineReducers({
    btnInputReducer: btnInputReducer,
});

export default reducers;

