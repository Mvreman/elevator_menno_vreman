import { RAISE } from '../actionTypes';

//calls the button action and gives it to the right data value
export const lift = (e) => dispatch => {
  const name = e.target.getAttribute('name');
  dispatch({ type: RAISE, name });
};

