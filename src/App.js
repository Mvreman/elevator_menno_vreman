import React from 'react';
import Button from './components/Button';
import Floors from './components/Floors';
import Elevator from './components/Elevator/elevator';
import ButtonIndicator from "./components/Button/ButtonIndicator";
import ElevatorData from "./redux/data/data";
import './Generalstyling.css';
import { Row, Col} from 'react-bootstrap';


const App = () => {

  return (
      <div>
          <div className="header">
              <h2>Elevator</h2>
          </div>
          <div className="container">
              <Row>
                  <Col>
                      {/*Control panel componemt*/}
                      <div className="control-panel">
                          <h2 className="cp_title">Control panel</h2>
                          <div className="button-container">
                          {ElevatorData.map((elevator, nbr) => (
                                  <Button key={nbr} name={elevator.name} value={elevator.name}/>
                              ))}
                          </div>
                      </div>
                  </Col>
                  <Col>
                      {/*Building + elevator*/}
                    <div className={"container-elevator"}>
                      <div className={"building"}>
                          {ElevatorData.map((container, nbr) => (
                                  <Floors />))}
                          <Elevator/>
                      </div>
                    </div>

                  </Col>
                  <Col>
                      {/*indicators*/}
                      <div className={"btn_container"}>
                          {ElevatorData.map((elevator, nbr) => (
                              <ButtonIndicator key={nbr} name={elevator.name} value={elevator.name}/>
                          ))}
                      </div>
                  </Col>

              </Row>
          </div>
      </div>
  )
};

export default App;
